# The Pokemon Trainer
A SPA angular ts Pokemon Trainer game. 
## Table of Contents
- [Description](#Description)
- [Installation](#Installation)
- [Components](#Components)
- [Maintainer](#Maintainer)
- [Author](#Author)
- [License](#License)
## Description
This is a single page application useing angular where you can creat a trainer and catch pokemon to your collection.
To start add a trainer name and press "Login", then catch pokemon by pressing the "Catch poemon" button.
Press the trainer button at the top right corner, to see your pokemon collection.
If you press the "Remove pokemon" button, the pokemon will be removed for your collection
The data for the trainers and the pokemon is provided to with RESTful API.
## Installation
Install node.js and install 
Clone the repo. Open with Visual studio code and the type "npm install" and  "npm i -g @angular/cli" in the terminal.
Then type "ng serve" in the terminal

## Components
This project contains components, services, pages, hoc, utils and guards, models and enums .
Also using router
## App Link
https://stormy-anchorage-78643.herokuapp.com/
## Maintainer
[@emiliozimberlin](https://gitlab.com/emiliozimberlin)
## Author
Emilio Zimberlin
## License
[MIT](https://choosealicense.com/licenses/mit/)

