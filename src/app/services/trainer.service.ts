import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  private _trainer?: Trainer;
  public get trainer(): Trainer | undefined {
    return this._trainer;
  }
  public set trainer(trainer: Trainer | undefined) {
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer!);
    this._trainer = trainer;
  }

  constructor() {
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
  }
  // checks if pokemon is collected
  public isCollected(name: string): boolean {
    if (this._trainer) {
      return Boolean(
        this.trainer?.pokemon.find(
          (hasPokemon: Pokemon) => hasPokemon.name === name
        )
      );
    }
    return false;
  }
  //  adds the pokemon form the collection
  public addToCollection(pokemonToAdd: Pokemon): void {
    if (this._trainer) {
      this._trainer.pokemon.push(pokemonToAdd);
    }
  }
  // removes the pokemon form the collection
  public removeFromCollection(name: string): void {
    if (this._trainer) {
      this._trainer.pokemon = this._trainer.pokemon.filter(
        (pokemon: Pokemon) => pokemon.name !== name
      );
    }
  }
}
