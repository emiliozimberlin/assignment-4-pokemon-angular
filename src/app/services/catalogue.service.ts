import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon, PokemonResponse } from '../models/pokemon.model';
import { StorageUtil } from '../utils/storage.util';

const { apiPokemon, apiPokemonImage } = environment;

@Injectable({
  providedIn: 'root',
})
export class CatalogueService {
  private _pokemonList: Pokemon[] = [];
  private _error: string = '';
  private _loading: boolean = false;

  public set PokemonList(pokemon: Pokemon[] | undefined) {
    StorageUtil.storageSave<Pokemon[]>(StorageKeys.Pokemon, pokemon!);
    this._pokemonList = pokemon!;
  }

  public get PokemonList(): Pokemon[] {
    return this._pokemonList;
  }
  public get error(): string {
    return this._error;
  }
  public get loading(): boolean {
    return this._loading;
  }

  constructor(private readonly http: HttpClient) {}

  // gets all pokemon
  public findAllPokemon(): void {
    if (this._pokemonList.length > 0 || this.loading) {
      return;
    }
    this._loading = true;
    this.http
      .get<PokemonResponse>(apiPokemon)
      .pipe(
        map((response: PokemonResponse) => {
          return response.results;
        })
      )
      .subscribe({
        next: (pokemon: Pokemon[]) => {
          StorageUtil.storageSave<Pokemon[]>(StorageKeys.Pokemon, pokemon!);
          this._pokemonList = pokemon;
          this.createImgUrl();
          this._loading = false;
        },
      });
  }

  // creates the images url
  public createImgUrl(): void {
    this._pokemonList.forEach((element) => {
      let imgUrl = element.url.split('/');
      element.imgUrl = apiPokemonImage + imgUrl[imgUrl.length - 2] + '.png';
    });
  }
  // finds a pokemon by name
  public findPokemonByName(name: string): Pokemon | undefined {
    return this._pokemonList.find((pokemon: Pokemon) => pokemon.name === name);
  }
}
