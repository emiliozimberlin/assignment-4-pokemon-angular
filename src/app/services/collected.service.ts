import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { CatalogueService } from './catalogue.service';
import { TrainerService } from './trainer.service';

const { apiTrainers, apiKey } = environment;

@Injectable({
  providedIn: 'root',
})
export class CollectedService {
  public loading: boolean = false;

  constructor(
    private readonly catalogueService: CatalogueService,
    private readonly trainerService: TrainerService,
    private readonly http: HttpClient
  ) {}
  // adds pokemon th the trainers collection and saves it the the api
  public addToCollection(name: string): Observable<Trainer> {
    if (!this.trainerService.trainer) {
      throw new Error('addToCollection: There is no trainer!');
    }
    const trainer: Trainer = this.trainerService.trainer;
    const selectedPokemon: Pokemon | undefined =
      this.catalogueService.findPokemonByName(name);
    if (!selectedPokemon) {
      throw new Error(
        'addToCollection: There is no pokemon with name: ' + name
      );
    }

    if (this.trainerService.isCollected(name)) {
      this.trainerService.removeFromCollection(name);
    } else {
      this.trainerService.addToCollection(selectedPokemon);
    }
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': apiKey,
    });
    return this.http
      .patch<Trainer>(
        `${apiTrainers}/${trainer.id}`,
        {
          pokemon: [...trainer.pokemon],
        },
        { headers }
      )
      .pipe(
        tap((UpdatedTrainer: Trainer) => {
          this.trainerService.trainer = UpdatedTrainer;
        }),
        finalize(() => {})
      );
  }
}
