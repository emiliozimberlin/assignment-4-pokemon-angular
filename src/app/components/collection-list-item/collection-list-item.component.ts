import { Component, OnInit, Input } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';

@Component({
  selector: 'app-collection-list-item',
  templateUrl: './collection-list-item.component.html',
  styleUrls: ['./collection-list-item.component.css']
})
export class CollectionListItemComponent implements OnInit {
  @Input() pokemon?: Pokemon
  @Input() i?: number
  constructor() { }

  ngOnInit(): void {
  }

}
