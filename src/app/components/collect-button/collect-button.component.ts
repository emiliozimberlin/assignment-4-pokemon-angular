import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { CollectedService } from 'src/app/services/collected.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-collect-button',
  templateUrl: './collect-button.component.html',
  styleUrls: ['./collect-button.component.css'],
})
export class CollectButtonComponent implements OnInit {
  
  public isCollected: boolean = false;
  public loading: boolean = false;
  @Input() pokemonName: string = '';

 
  constructor(
    private readonly collectedService: CollectedService,
    private readonly trainerService: TrainerService
  ) {}

  ngOnInit(): void {
    this.isCollected = this.trainerService.isCollected(this.pokemonName);
    if(this.isCollected){
    }
  }
  // adds the selected pokemon to the trainers collection
  public onCatchClick(): void {
    this.loading = true;
    this.collectedService.addToCollection(this.pokemonName).subscribe({
      next: (trainer: Trainer) => {
        this.isCollected = this.trainerService.isCollected(this.pokemonName);
        this.loading = false;
      },
      error: (error: HttpErrorResponse) => {
        console.log('ERROR ' + error.message);
      },
    });
  }
}
