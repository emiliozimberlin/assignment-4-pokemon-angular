import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { CatalogueService } from 'src/app/services/catalogue.service';
import { TrainerService } from 'src/app/services/trainer.service';
import { StorageUtil } from 'src/app/utils/storage.util';

@Component({
  selector: 'app-logout-button',
  templateUrl: './logout-button.component.html',
  styleUrls: ['./logout-button.component.css'],
})
export class LogoutButtonComponent implements OnInit {
  constructor(
    private readonly trainerService: TrainerService,
    private readonly pokemonService: CatalogueService,
    private router: Router
  ) {}

  ngOnInit(): void {}
  // logout the trainer and removes the storages
  public logout(): void {
    this.trainerService.trainer = undefined;
    StorageUtil.storageRemove(StorageKeys.Trainer, this.trainerService.trainer);
    StorageUtil.storageRemove(
      StorageKeys.Pokemon,
      this.pokemonService.PokemonList
    );
    this.router.navigateByUrl('/login');
  }
}
