import { Component, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Trainer } from 'src/app/models/trainer.model';
import { LoginService } from 'src/app/services/login.service';
import { TrainerService } from 'src/app/services/trainer.service';


@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent{

  public loading: boolean = false;
  @Output() login: EventEmitter<void> = new EventEmitter()

  constructor(private readonly loginService: LoginService, private readonly userService: TrainerService) { }

  // logins in the trainer or creates a trainer if trainer dose not exist
  public loginSubmit(loginForm: NgForm): void {
    this.loading = true;
    const {username} = loginForm.value;
    this.loginService.login(username)
    .subscribe({
      next: (trainer: Trainer) =>{
        this.loading = false;
        this.userService.trainer = trainer;
        this.login.emit()
        
      },
      error: () =>{

      },
    })
  }
  

}
