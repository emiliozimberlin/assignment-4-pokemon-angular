import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { CatalogueService } from 'src/app/services/catalogue.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.css']
})
export class CataloguePage implements OnInit {

  public get pokemonList(): Pokemon[]{
    return this.catalogueService.PokemonList;
  }

  public get loading(): boolean{
    return this.catalogueService.loading;
  }

  public get error(): string{
    return this.catalogueService.error;
  }
  

  constructor(private readonly catalogueService: CatalogueService) { }

  ngOnInit(): void {
    
    this.catalogueService.findAllPokemon();
  }

}
