import { Component, OnInit,  } from '@angular/core';
import { Router } from '@angular/router';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css'],
})
export class LoginPage implements OnInit {
  get trainer(): Trainer | undefined {
    return this.trainerService.trainer
  }


  constructor(private readonly router: Router, private readonly trainerService: TrainerService) {}

  ngOnInit(): void {
    if(this.trainerService.trainer){
      this.router.navigateByUrl('/catalogue');
    }
  }


  handleLogin(): void {
    this.router.navigateByUrl('/catalogue');
  }
}
