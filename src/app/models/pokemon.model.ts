export interface PokemonResponse{
    count: number;
    nest: string;
    results: Pokemon[]
}

export interface Pokemon {
  name: string;
  url: string;
  imgUrl: string;
}
