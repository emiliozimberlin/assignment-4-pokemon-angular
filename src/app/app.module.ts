import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule} from "@angular/common/http"
import { AppComponent } from './app.component';
import { LoginPage } from './pages/login/login.page';

import { TrainerPage } from './pages/trainer/trainer.page';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { FormsModule } from '@angular/forms';
import { CataloguePage } from './pages/catalogue/catalogue.page';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { PokemonListItemComponent } from './components/pokemon-list-item/pokemon-list-item.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CollectButtonComponent } from './components/collect-button/collect-button.component';
import { CollectionListComponent } from './components/collection-list/collection-list.component';
import { CollectionListItemComponent } from './components/collection-list-item/collection-list-item.component';
import { CollectionCardComponent } from './components/collection-card/collection-card.component';
import { LogoutButtonComponent } from './components/logout-button/logout-button.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    TrainerPage,
    LoginFormComponent,
    CataloguePage,
    PokemonListComponent,
    PokemonListItemComponent,
    NavbarComponent,
    CollectButtonComponent,
    CollectionListComponent,
    CollectionListItemComponent,
    CollectionCardComponent,
    LogoutButtonComponent
      ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule

  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
