import { Component, OnInit } from '@angular/core';
import { CatalogueService } from './services/catalogue.service';
import { TrainerService } from './services/trainer.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'assignment-4-pokemon-angular';

  constructor(private readonly trainerService: TrainerService, private readonly pokemonService: CatalogueService){

  }

  ngOnInit(){
    if(this.trainerService.trainer){
      this.pokemonService.findAllPokemon();
    }
  }
}
